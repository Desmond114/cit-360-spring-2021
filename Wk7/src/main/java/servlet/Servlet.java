package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns ={"/Servlet"})
public class Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String noun1 = request.getParameter("noun1");
        String adj = request.getParameter("adj");
        String noun2 = request.getParameter("noun2");
        out.println("<h1>The Mona Lisa</h1>");
        out.println("<p>After hiding the painting in his " + noun1 + " for two years,</p>");
        out.println("<p>he grew " + adj + " and tried to sell it</p>");
        out.println("<p>to a(n) " + noun2 + " in Florence, but was caught.</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available at this time.");
    }
}
