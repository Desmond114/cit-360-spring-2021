<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Wk7- Servlets</title>
</head>
<body>
<h1><%="Servlet MadLib"%></h1>
<h2><%= "Testing to see if I can build a servlet." %></h2>
<br/>
<p>To launch MadLib, click <a href="${pageContext.request.contextPath}/madLib.html">here.</a></p>
</body>
</html>