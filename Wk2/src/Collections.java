
import java.util.*;

public class Collections {
    public static void main(String[] args) {
        //list
        System.out.println("** Example for List **");

        List<String> veggies = new ArrayList<String>();
        veggies.add("Squash");
        veggies.add("Tomato");
        veggies.add("Onion");
        veggies.add("Celery");

        for (Object str : veggies) {
            System.out.println((String) str);
        }

        //queue
        System.out.println("** Example for Queue **");

        Queue<String> fruits = new PriorityQueue<String>();
        fruits.add("Dragon Fruit");
        fruits.add("tomato");
        fruits.add("grape");
        fruits.add("Strawberry");

        Iterator nextFruit = fruits.iterator();
        while (nextFruit.hasNext()) {
            System.out.println(fruits.poll());
        }

        //set
        System.out.println("** Example for Set **");

        Set<Integer> grades = new HashSet<Integer>();
        grades.add(56);
        grades.add(18);
        grades.add(100);
        grades.add(70);
        grades.add(18);
        grades.add(36);

        for (Object ints : grades) {
            System.out.println((Integer) ints);
        }

        //tree
        System.out.println("** Example for TreeSet **");
        Set<Integer> gradeTree = new TreeSet<Integer>();
        gradeTree.add(56);
        gradeTree.add(18);
        gradeTree.add(100);
        gradeTree.add(70);
        gradeTree.add(18);
        gradeTree.add(36);

        for (Object ints : gradeTree) {
            System.out.println((Integer) ints);
        }

        //map
        System.out.println("** Example for Map **");
        Map<Integer, String> codeMsg = new HashMap<>();
        codeMsg.put(1, "i");
        codeMsg.put(13, "e");
        codeMsg.put(3, "l");
        codeMsg.put(8, "t");
        codeMsg.put(11, "v");
        codeMsg.put(14, "s");
        codeMsg.put(4, "i");
        codeMsg.put(9, "e");
        codeMsg.put(5, "k");
        codeMsg.put(11, "t");
        codeMsg.put(9, "u");
        codeMsg.put(6, "e");
        codeMsg.put(12, "l");
        codeMsg.put(10, "r");

        for (int i = 1; i < 15; i++) {
            String crackCode = (String) codeMsg.get(i);
            if (crackCode == null)
                System.out.println(" ");
            else
                System.out.print(crackCode);
        }
    }
}
