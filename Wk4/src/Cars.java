import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Cars {
    private String make;
    private String model;
    private int year;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString() {
        return "Car's Make: " + make + ", Model: " +
                model + ", Year: " + year;
    }

    public static Cars JSONToCars(String s) {
        //Calls server component, gets JSON, and converts
        //JSON parser into an object

        //Convert JSON to object
        ObjectMapper mapper = new ObjectMapper();
        Cars carInfo = null;

        try {
            carInfo = mapper.readValue(s, Cars.class);
        } catch (JsonProcessingException ex) {
            System.err.println(ex.toString());
        }

        return carInfo;
    }

    public static void main(String[] args) {
        Cars carObj = new Cars();
        carObj.setMake("Chevy");
        carObj.setModel("Malibu");
        carObj.setYear(2006);

        //Sent HTTP Get request
        String webInfo = httpServer.urlInfo();
        //Print out HTTP
        System.out.println(webInfo);

        //Gets JSON data from Object
        String information = httpServer.carsToJSON(carObj);
        System.out.println("--- This is the JSON data: ");
        //Prints out Object to JSON
        System.out.println(information);

        //Gets Object data from JSON
        Cars carObj2 = Cars.JSONToCars(information);
        System.out.println("--- This is the object data: ");
        //Print out JSON to Object
        System.out.println(carObj2);
    }
}
