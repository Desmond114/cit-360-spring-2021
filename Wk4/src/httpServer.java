import java.io.BufferedReader;
import java.io.*;
import java.net.*;
import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class httpServer {
    // Takes object and converts it to JSON using parser
    // returns a block of JSON using HTTP

    public static String getContentString(String string) {

        //Initialize string variable
        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            StringBuilder builder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line + "\n");
            }

            content = builder.toString();
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }

        return content;
    }

    public static Map getHttpHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;
    }

    public static String urlInfo() {
        String r = "--- Above is the information for the keys from Yahoo.com ---";
        System.out.println(httpServer.getContentString("http://www.yahoo.com"));

        Map<Integer, List<String>> m = httpServer.getHttpHeaders("http://www.yahoo.com");


        for (Map.Entry<Integer, List<String>> entry : m.entrySet()) {
            try {
                System.out.println("Key= " + entry.getKey() + entry.getValue());
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
        return r;
    }

    public static String carsToJSON(Cars carInfo) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(carInfo);
        } catch (JsonProcessingException ex) {
            System.err.println(ex.toString());
        }

        return s;
    }
}
