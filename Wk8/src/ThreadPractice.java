public class ThreadPractice implements Runnable{

    private String name;
    private int turn;
    private int spaces;

    public ThreadPractice (String name, int turn, int spaces){
         this.name = name;
         this.turn = turn;
         this.spaces = spaces;
    }

    @Override
    public void run() {
        System.out.println("It is " + name +"'s " + turn + " turn to move " + spaces + ".\n\n");

        for (int i = 1; i < 7; i++){
            if ( i % turn == 0){
                System.out.println(name + "'s turn.");
                try{
                    Thread.sleep(turn);
                }
                catch (InterruptedException ex){
                    System.out.println("Program was interrupted.");
                }
            }
        }

        System.out.println(name + "'s turn is over.");
    }
}
