import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThreadPractice {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        ThreadPractice tp1 = new ThreadPractice("Mary", 5, 3);
        ThreadPractice tp2 = new ThreadPractice("Clark", 3, 6);
        ThreadPractice tp3 = new ThreadPractice("Tom", 6, 8);
        ThreadPractice tp4 = new ThreadPractice("Wendy", 2, 2);
        ThreadPractice tp5 = new ThreadPractice("Crispin", 4, 5);
        ThreadPractice tp6 = new ThreadPractice("Steve", 1, 4);

        myService.execute(tp1);
        myService.execute(tp2);
        myService.execute(tp3);
        myService.execute(tp4);
        myService.execute(tp5);
        myService.execute(tp6);

        myService.shutdown();
    }
}
