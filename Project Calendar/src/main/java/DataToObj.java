import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class DataToObj {
    SessionFactory factory = null;
    Session session = null;

    private static DataToObj single_instance = null;

    private DataToObj() {
        factory = HibernateUtil.getSessionFactory();
    }

    /**
     * This is what makes this class a singleton.  You use this
     * class to get an instance of the class.
     */
    public static DataToObj getInstance() {
        if (single_instance == null) {
            single_instance = new DataToObj();
        }

        return single_instance;
    }

    /**
     * Used to get more than one customer from database
     * Uses the OpenSession construct rather than the
     * getCurrentSession method so that I control the
     * session.  Need to close the session myself in finally.
     */
    public List<Baseball_Events> getBaseball_Events() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Baseball_Events";
            List<Baseball_Events> be = (List<Baseball_Events>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return be;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

}
