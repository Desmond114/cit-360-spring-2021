package HibernateTest;

import javax.persistence.*;

@Entity
@Table(name = "baseball_events")
public class Baseball_Events {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "event_date")
    private String eDate;

    @Column(name = "event_time")
    private String eTime;

    @Column(name = "location")
    private String location;

    @Column(name = "baseball_team1")
    private String team1;

    @Column(name = "baseball_team2")
    private String team2;

    @Column(name = "pitch_counter")
    private String pCounter;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String geteDate() {
        return eDate;
    }

    public void seteDate(String eDate) {
        this.eDate = eDate;
    }


    public String geteTime() {
        return eTime;
    }

    public void seteTime(String eTime) {
        this.eTime = eTime;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }


    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getpCounter() {
        return pCounter;
    }

    public void setpCounter(String pCounter) {
        this.pCounter = pCounter;
    }

    public String toString() {
        return Integer.toString(id) + " || " + eDate + " || " + eTime + " || " + location + " || " + team1 + " vs "
                + team2 + " || " + pCounter;
    }
}


