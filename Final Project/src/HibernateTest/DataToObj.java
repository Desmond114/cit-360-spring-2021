package HibernateTest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.*;

public class DataToObj {
    SessionFactory factory = null;
    Session session = null;

    private static DataToObj single_instance = null;

    private DataToObj() {
        factory = HibernateUtil.getSessionFactory();
    }

    /**
     * This is what makes this class a singleton.  You use this
     * class to get an instance of the class.
     */
    public static DataToObj getInstance() {
        if (single_instance == null) {
            single_instance = new DataToObj();
        }

        return single_instance;
    }

    /**
     * Used to get more than one customer from database
     * Uses the OpenSession construct rather than the
     * getCurrentSession method so that I control the
     * session.  Need to close the session myself in finally.
     */
    public List<Baseball_Events> getBaseball_Events() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateTest.Baseball_Events";
            List<Baseball_Events> be = (List<Baseball_Events>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return be;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /**
     * Used to get a single customer from database
     */
    public Baseball_Events getBaseball_Events(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateTest.Baseball_Events where id=" + Integer.toString(id);
            Baseball_Events b = (Baseball_Events) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return b;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
