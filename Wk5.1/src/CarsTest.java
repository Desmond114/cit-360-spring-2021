import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarsTest {

    @Test
    void getMake() {
        Cars c = new Cars("Toyota", "Rav4", 2016);
        assertEquals("Toyota", c.getMake());
    }

    @Test
    void getModel() {
        Cars c = new Cars("Ford", "Mustang", 1969);
        assertEquals("Mustang", c.getModel());
    }

    @Test
    void getYear() {
        Cars c = new Cars("Honda", "Civic", 2009);
        assertEquals(2009, c.getYear());
    }

}