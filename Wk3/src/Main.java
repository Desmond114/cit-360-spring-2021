import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //Describe program and prompt user for input
        System.out.println("This program will divide two whole numbers for you and will display an exception.");

        //initiate variables
        boolean oops = true;
        int firstNum = 0;
        int secondNum = 0;

        //Loop to allow for re-entry if exception thrown
        do {
            //Try/Catch for ArithmeticException
            try {
                System.out.println("Please enter the first number: ");
                firstNum = input.nextInt();
                System.out.println("Please enter the second number: ");
                secondNum = input.nextInt();

                //Print out users results and calling method Division
                System.out.println("For Exception example: If you divide " + firstNum + " by " +
                        secondNum + ", you get " + division(firstNum, secondNum));
                oops = false;
            } catch (ArithmeticException ex) {
                System.out.println("Exception: Cannot divide by Zero. Please try again.");
            }
        }
        while (oops);

        // Running code to show data validation
        System.out.println("This program will divide two whole numbers for you and will display data validation.");

        System.out.println("Please enter the first number: ");
        firstNum = input.nextInt();
        System.out.println("Please enter the second number: ");
        secondNum = input.nextInt();
        while (secondNum == 0) {
            System.out.println("Data Validation: Cannot divide by Zero. Please try again.");
            System.out.println("Please enter the second number: ");
            secondNum = input.nextInt();
        }
        System.out.println("For data validation example: If you divide " + firstNum + " by " +
                secondNum + ", you get " + division(firstNum, secondNum));
    }

    public static float division(int firstNum, int secondNum) {
        //Test if thrown exception
        if (secondNum == 0) {
            throw new ArithmeticException("Cannot divide by 0");
        }

        //Divide numbers given
        float total = (float) firstNum / (float) secondNum;

        return total;
    }
}
